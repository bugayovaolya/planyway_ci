const testBoard = {
    id: '6088cf0d4f464b4e3bee9f3a',
    name: 'Software Development [Web App, iOS App, Android App]',

    testList: {
        name: 'Backlog',
        id: '6088cf0d4f464b4e3bee9f3c'
    },
    testCard: {
        name: 'New card',
        id: '60d861cd2343ce0978367324'
    },
    newName: {
        name: 'Test',
    },
};

const testData = {
    boardNames: [
        'Board 1',
        'Board 2',
        'Board 3',
        'Board 4',
        'Board 5',
        'Board 6',
        'Board 7'
    ],
    listName: 'New List',
    cardNames: [
        'Card 1',
        'Card 2',
        'Card 3',
        'Card 4',
        'Card 5'
    ],
}



export { testBoard, testData }